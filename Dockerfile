FROM golang:1.14-alpine AS builder

### add make gcc g++ python if you will use native dependencies
RUN mkdir -p $GOPATH/src/gitlab.com/Jota_cepeda/proof_user && \
apk add --no-cache gcc g++ git openssh-client

WORKDIR $GOPATH/src/gitlab.com/Jota_cepeda/proof_user

ENV GO111MODULE=on

COPY . .

WORKDIR $GOPATH/src/gitlab.com/Jota_cepeda/proof_user

RUN go build ./... && go build

FROM alpine

RUN apk --no-cache add ca-certificates bash tzdata

WORKDIR /root/

COPY --from=builder /go/src/gitlab.com/Jota_cepeda/proof_user .

ENV DB_ENGINE=mongoDB
ENV DB_HOST=localhost
ENV DB_PORT=27017
ENV DB_DATABASE=Inventary
ENV DB_STRING_CONNECTION=mongodb://127.0.0.1:27017

# GENERAL ENV FOR  APP
ENV APP_PORT=8080

RUN chmod +x ./proof_user

EXPOSE $APP_PORT

ENTRYPOINT [ "./proof_user" ]
