module gitlab.com/Jota_cepeda/proof_user

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.6.0
)
