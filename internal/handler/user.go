package handler

import (
	"net/http"

	"github.com/gorilla/mux"
	processor "gitlab.com/Jota_cepeda/proof_user/internal/user"
	"gitlab.com/Jota_cepeda/proof_user/tools"
)

// CreateUserHandler - POST - /api/v1/user
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.CreateUserProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}

// GetUserHandler - GET - /api/v1/user/{id}
func GetUserHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.GetUserProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}

// UpdateUserHandler - PUT - /api/v1/user/update/{id}
func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.UpdateUserProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)

}

// DeleteUserHandler - DELETE - /api/v1/user/delete/{id}
func DeleteUserHandler(w http.ResponseWriter, r *http.Request) {

	_, err := processor.DeleteUserProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, false, err, 500)
		return
	}

	tools.HandlersResponse(w, true, nil, 200)
}

// MESSAGE
// CreateMessageHandler - POST - /api/v1/message/
func CreateMessageHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.CreateMessageProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}

// GetMessagesHandler - GET - /api/v1/message/{id}
func GetMessagesHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.GetMessageProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}
	tools.HandlersResponse(w, resp, nil, 200)
}

// GetAllMessagesByUserHandler - GET - /api/v1/message/{id}
func GetAllMessagesByUserHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.GetAllMessageByUserProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}

// UpdateMessageHandler - PUT - /api/v1/message/update/{id}
func UpdateMessageHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := processor.UpdateMessageProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}

// DeleteMessageHandler - DELETE - /api/v1/message/delete/{id}
func DeleteMessageHandler(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	k := vars["id"]

	_, err := processor.DeleteMessageProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, "Record successfully deleted: "+k, nil, 200)
}
