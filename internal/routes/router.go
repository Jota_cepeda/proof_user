package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/Jota_cepeda/proof_user/internal/handler"
)

func Router(r *mux.Router) {
	// User
	r.HandleFunc("/api/v1/user", handler.CreateUserHandler).Methods("POST")
	r.HandleFunc("/api/v1/user/{id}", handler.GetUserHandler).Methods("GET")
	r.HandleFunc("/api/v1/user/update/{id}", handler.UpdateUserHandler).Methods("PUT")
	r.HandleFunc("/api/v1/user/delete/{id}", handler.DeleteUserHandler).Methods("DELETE")

	// POST
	r.HandleFunc("/api/v1/message", handler.CreateMessageHandler).Methods("POST")
	r.HandleFunc("/api/v1/message/getall/byuser/{userid}", handler.GetAllMessagesByUserHandler).Methods("GET")
	r.HandleFunc("/api/v1/message/get/{id}", handler.GetMessagesHandler).Methods("GET")
	r.HandleFunc("/api/v1/message/update/{id}", handler.UpdateMessageHandler).Methods("PUT")
	r.HandleFunc("/api/v1/message/delete/{id}", handler.DeleteMessageHandler).Methods("DELETE")

}
