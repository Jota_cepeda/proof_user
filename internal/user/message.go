package user

// User model
type Message struct {
	ID     string `json:"_id,omitempty" bson:"_id"`
	UserID string `json:"user_id,omitempty" bson:"user_id"`
	Titulo string `json:"titulo,omitempty" bson:"titulo"`
	Body   string `json:"body,omitempty" bson:"body"`
}
