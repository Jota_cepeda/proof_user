package user

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

//Message
// CreateMessageProcessor ...
func CreateMessageProcessor(w http.ResponseWriter, r *http.Request) (*Message, error) {
	fmt.Println("CreateMessageProcessor - start")
	message := &Message{}

	if err := json.NewDecoder(r.Body).Decode(message); err != nil {
		return message, err
	}

	err := messageStorage.InsertMessage(message)
	if err != nil {
		return message, err
	}
	fmt.Println("CreateMessageProcessor - end")
	return message, nil
}

// GetMessageProcessor ...
func GetMessageProcessor(w http.ResponseWriter, r *http.Request) (*Message, error) {

	message := &Message{}

	vars := mux.Vars(r)
	id := vars["id"]

	message, err := messageStorage.GetMessage(id)
	if err != nil {
		return nil, err
	}

	return message, nil
}

// GetAllMessageProcessor ...
func GetAllMessageByUserProcessor(w http.ResponseWriter, r *http.Request) ([]*Message, error) {

	vars := mux.Vars(r)
	userID := vars["userid"]

	user, err := messageStorage.GetAllMessageByUser(userID)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// UpdateMessageProcessor ...
func UpdateMessageProcessor(w http.ResponseWriter, r *http.Request) (*Message, error) {

	message := &Message{}

	vars := mux.Vars(r)
	id := vars["id"]

	err := json.NewDecoder(r.Body).Decode(message)
	if err != nil {
		return message, err
	}

	message, err = messageStorage.UpdateMessage(id, message)
	if err != nil {
		return message, err
	}

	return message, nil
}

// DeleteMessageProcessor ...
func DeleteMessageProcessor(w http.ResponseWriter, r *http.Request) (bool, error) {

	vars := mux.Vars(r)
	id := vars["id"]

	_, err := messageStorage.DeleteMessage(id)
	if err != nil {
		return false, err
	}

	return true, nil
}
