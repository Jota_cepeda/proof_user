package user

// User model
type User struct {
	ID                 string `json:"_id,omitempty" bson:"_id"`
	Nombre             string `json:"nombre,omitempty" bson:"nombre"`
	Apellido           string `json:"apellido,omitempty" bson:"apellido"`
	TipoIdentificacion string `json:"tipo_identificacion,omitempty" bson:"tipo_identificacion"`
	Edad               string `json:"edad,omitempty" bson: "edad"`
}
