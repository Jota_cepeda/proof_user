package user

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

// User

// CreateUserProcessor ...
func CreateUserProcessor(w http.ResponseWriter, r *http.Request) (*User, error) {

	user := &User{}

	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		return user, err
	}

	//TODO CREATE
	err := userStorage.InsertUser(user)
	if err != nil {
		return user, err
	}

	return user, nil
}

// GetUserProcessor ...
func GetUserProcessor(w http.ResponseWriter, r *http.Request) (*User, error) {

	vars := mux.Vars(r)
	id := vars["id"]

	user, err := userStorage.GetUser(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// UpdateUserProcessor ...
func UpdateUserProcessor(w http.ResponseWriter, r *http.Request) (*User, error) {

	user := &User{}

	vars := mux.Vars(r)
	id := vars["id"]

	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		return user, err
	}

	user, err = userStorage.UpdateUser(id, user)
	if err != nil {
		return user, err
	}

	return user, nil
}

// DeleteUserProcessor ...
func DeleteUserProcessor(w http.ResponseWriter, r *http.Request) (bool, error) {

	vars := mux.Vars(r)
	id := vars["id"]

	_, err := userStorage.DeleteUser(id)
	if err != nil {
		return false, err
	}

	return true, nil
}

/*
func AuthenticationUser(w http.ResponseWriter, r *http.Request) (string, error) {

	resp := Response{}
	auth := Model{}
	resp.Error = false
	resp.Codigo = ""
	resp.Mensaje = ""

	err := json.NewDecoder(r.Body).Decode(&auth)
	if err != nil {
		fmt.Println("the request could not be made - POST", err)
		w.WriteHeader(http.StatusNoContent)
		resp.Error = true
		resp.Codigo = "400"
		resp.Mensaje = "the request could not be made - POST"
	}

	//TODO Busca usuario

	// Genera Token
	token, err := GenerateJWT(auth)
	if err != nil {
		fmt.Println("generando el token del usuario: %v", err)
		return "", err
	}

	return token, nil
}

var (
	signKey   *rsa.PrivateKey
	verifyKey *rsa.PublicKey
)

type jwtCustomClaims struct {
	User      Model  `json:"user"`
	IPAddress string `json:"ip_address"`
	jwt.StandardClaims
}

func GenerateJWT(u Model) (string, error) {
	c := &jwtCustomClaims{
		User:      u,
		IPAddress: "localhost",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			Issuer:    "Ecatch",
		},
	}

	t := jwt.NewWithClaims(jwt.SigningMethodRS256, c)
	token, err := t.SignedString(signKey)
	if err != nil {
		fmt.Printf("firmando el token: %v", err)
		return "", err
	}

	return token, nil
}
*/
