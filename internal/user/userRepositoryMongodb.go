package user

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type userMongoDB struct{}

var (
	dataBaseName   string = "Inventory"
	collectionName string = "users"
	client         *mongo.Client
)

func init() {
	var err error
	client, err = getConnection()
	if err != nil {
		log.Fatalln(err)
	}
}

// InsertUser ...
func (s userMongoDB) InsertUser(data *User) error {

	err := userWorkMongoDBInsertData(data)
	if err != nil {
		return err
	}

	return nil
}

// GetUser ...
func (s userMongoDB) GetUser(ID string) (*User, error) {

	userDB, err := userWorkMongoDBGetOneInstanceData(ID)
	if err != nil {
		return nil, err
	}

	return userDB, nil
}

// UpdateUser ...
func (s userMongoDB) UpdateUser(ID string, user *User) (*User, error) {

	data, err := json.Marshal(user)
	if err != nil {
		return nil, err
	}

	updateData := make(map[string]interface{})

	err = json.Unmarshal(data, &updateData)
	if err != nil {
		return nil, err
	}

	user, err = userWorkMongoDBUpdateOneInstanceData(ID, updateData)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// DeleteUser ...
func (s userMongoDB) DeleteUser(ID string) (bool, error) {

	err := userWorkMongoDBDeleteOneInstanceData(ID)
	if err != nil {
		return false, err
	}

	return true, nil
}

// getConnection ...
func getConnection() (*mongo.Client, error) {

	stringConnection := os.Getenv("DB_STRING_CONNECTION")

	clientOpts := options.Client().ApplyURI(fmt.Sprintf(stringConnection))

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		return nil, err
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// userWorkMongoDBInsertData ...
func userWorkMongoDBInsertData(data *User) error {

	collection := client.Database(dataBaseName).Collection(collectionName)

	id, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		return err
	}

	data.ID = id.InsertedID.(primitive.ObjectID).Hex()

	return nil
}

// userWorkMongoDBGetOneInstanceData ...
func userWorkMongoDBGetOneInstanceData(ID string) (*User, error) {

	user := &User{}

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return nil, err
	}

	filter := bson.D{{"_id", docID}}

	collection := client.Database(dataBaseName).Collection(collectionName)

	err = collection.FindOne(context.TODO(), filter).Decode(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// userWorkMongoDBUpdateOneInstanceData ...
func userWorkMongoDBUpdateOneInstanceData(ID string, data map[string]interface{}) (*User, error) {

	user := &User{}

	collection := client.Database(dataBaseName).Collection(collectionName)

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return user, err
	}

	filter := bson.D{{"_id", docID}}

	update := bson.M{}
	update = data

	result := collection.FindOneAndUpdate(context.TODO(), filter, bson.M{"$set": update}, options.FindOneAndUpdate().SetReturnDocument(1))
	if result.Err() != nil {
		return user, err
	}

	err = result.Decode(user)
	if err != nil {
		return user, err
	}

	user.ID = ID

	return user, nil
}

// userWorkMongoDBDeleteOneInstanceData ...
func userWorkMongoDBDeleteOneInstanceData(ID string) error {

	collection := client.Database(dataBaseName).Collection(collectionName)

	dataMesagge, err := messageStorage.GetAllMessageByUser(ID)
	if err != nil {
		return err
	}

	for _, value := range dataMesagge {

		_, err := messageStorage.DeleteMessage(value.ID)
		if err != nil {
			return err
		}

	}

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return err
	}

	filter := bson.D{{"_id", docID}}

	_, err = collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
