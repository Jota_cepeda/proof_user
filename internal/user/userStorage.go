package user

import (
	"fmt"
	"os"
	"strings"
)

var userStorage userStorageInterface

func init() {
	setUserStorage()
}

type userStorageInterface interface {
	InsertUser(user *User) error
	GetUser(ID string) (*User, error)
	UpdateUser(ID string, user *User) (*User, error)
	DeleteUser(ID string) (bool, error)
}

// setUserStorage ...
func setUserStorage() {

	dbEngine := os.Getenv("DB_ENGINE")

	switch strings.ToLower(dbEngine) {
	case "mongodb":
		userStorage = userMongoDB{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		fmt.Printf("este motor de bd no está configurado aún: %s", os.Getenv("DB_CONNECTION"))
	}

}
