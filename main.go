package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Jota_cepeda/proof_user/internal/routes"
)

// main: this a start for application
func main() {

	r := mux.NewRouter().StrictSlash(false)

	// Load http routes
	routes.Router(r)

	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8080"
	}

	server := &http.Server{
		Addr:           fmt.Sprintf(":%s", port),
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Println("Http Server Listen .. PORT: ", port)
	server.ListenAndServe()

}
