package tools

import (
	"encoding/json"
	"net/http"
)

// HandlersResponse ...
func HandlersResponse(w http.ResponseWriter, data interface{}, err interface{}, statusCode int) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	response := struct {
		Data  interface{} `json:"data"`
		Error interface{} `json:"error"`
	}{
		Data:  data,
		Error: err,
	}

	dataSerialized, err := json.Marshal(response)
	if err != nil {
		w.Write([]byte("Serialization error"))
		return
	}

	w.Write(dataSerialized)

}
